<?php
  include('functions.php');

  if($_REQUEST['id']) {
    $student = getArbol($_REQUEST['id']);
       
  }

  if($_POST){
    if ($filename = uploadPicture('picture')){
     
      $student['imagen'] = $filename;
      $student['especie'] = $_POST['especie'];
      $student['nombre'] = $_POST['nombre'];
      guardarArbolNuevo($student);
    } else {
      echo "There was an error saving the picture";
    }
  }
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administrador</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/admin.css">
    <link rel="shortcut icon" href="img/fIcon4.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js"></script> 
</head>

<body >
   <body >
        <div class="contenedor">
            <nav class="navbar navbar-expand-sm bg-pink navbar-light">
                <a class="navbar-brand" href="#">
                    <img src="img/logo1.png" alt="Logo" style="width:20%;">
                </a>
            </nav>
        </div>
            <header> </header>
            <input type="checkbox" id="check">
            <label for="check" class="icon-menu">Menu</label>
            
           
            
            <nav class="menu">
                <ul>
                    <li><a href="index.php">Inicio</a></li>
                    <li><a href="">Configuración</a></li>
                    <li><a href="">Mis arboles</a></li>
                    <li><a href="tienda.php">Tienda de arboles</a></li>
                    <li><a href="cerrarSesion.php">Cerrar sesión</a></li>
                   
                </ul>
            </nav>
            <div class="container mt-2">
                <div class="d-flex mb-3">
                    <div class="p2 flex-fill">
                        <div class="row">

                            <div class="col-sm">
                                <div class="cuentas">
                                     <table class="table table-light">
                                      <tbody>
                                        <tr>
                                          <td>Amigos</td>
                                          <td>Arboles</td>

                                        </tr>
                                        <?php
                                          $students = getProductos();
                                          $studentsHtml = "";
                                          foreach ($students as $student) {
                                            $studentsHtml .= 
                                            "<tr id='student_{$student['id']}'>
                                            <td >{$student['especie']}</td>
                                            <td >{$student['nombre']}</td>
                                            </tr>";
                                          }
                                          echo $studentsHtml;
                                        ?>
                                      </tbody>
                                    </table> 
                                </div>
                            </div>
                            <div class="col-sm">
                                <div class="cuentas">
                                       <table class="table table-light">
                                      <tbody>
                                        <tr>
                                          <td>Amigos</td>
                                          <td>Arboles</td>

                                        </tr>
                                        <?php
                                          $students = getProductos();
                                          $studentsHtml = "";
                                          foreach ($students as $student) {
                                            $studentsHtml .= 
                                            "<tr id='student_{$student['id']}'>
                                            <td >{$student['especie']}</td>
                                            <td >{$student['nombre']}</td>
                                            </tr>";
                                          }
                                          echo $studentsHtml;
                                        ?>
                                      </tbody>
                                    </table> 
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>


  

</body>

</html>