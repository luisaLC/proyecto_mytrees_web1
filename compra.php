<?php
  include('functions.php');

  if(isset($_POST['especie']) && isset($_POST['nombre']) && isset($_POST['montoDonar']) 
    && isset($_POST['imagen']), && isset($_POST['nombreDoador']) ) {
    $saved = saveArboles($_POST);
    

    if($saved) {
      header('Location: /Proyecto 1 - My Trees/?status=success');
    } else {
      header('Location: /Proyecto 1 - My Trees/?status=error');
    }
  } else {
    header('Location: /Proyecto 1 - My Trees/?status=error');