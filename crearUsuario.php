<?php
  include('functions.php');

  if(isset($_POST['nombre']) && isset($_POST['apellido']) && isset($_POST['telefono']) 
    && isset($_POST['correo']) && isset($_POST['direccion'])
    && isset($_POST['pais']) && isset($_POST['contraseña'])) {
    $saved = saveStudent($_POST);
    

    if($saved) {
      header('Location: /Proyecto 1 - My Trees/?status=success');
    } else {
      header('Location: /Proyecto 1 - My Trees/?status=error');
    }
  } else {
    header('Location: /Proyecto 1 - My Trees/?status=error');
  }