<?php
  include('functions.php');

  if($_REQUEST['id']) {
    $student = getArbol($_REQUEST['id']);
       
  }

  if($_POST){
    if ($filename = uploadPicture('picture')){
     
      $student['imagen'] = $filename;
      $student['especie'] = $_POST['especie'];
      $student['nombre'] = $_POST['nombre'];
      guardarArbolNuevo($student);
    } else {
      echo "There was an error saving the picture";
    }
  }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tienda</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/guardaArbol.css">
    <link rel="shortcut icon" href="img/fIcon4.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  
</head>

<body >
    <main>
        <div class="content-all">
            <header> </header>
            <input type="checkbox" id="check">
            <label for="check" class="icon-menu">Menu</label>
            
           
            
            <nav class="menu">
                <ul>
                    <li><a href="index.php">Inicio</a></li>
                    <li><a href="">Configuración</a></li>
                    <li><a href="">Mis arboles</a></li>
                    <li><a href="tienda.php">Tienda de arboles</a></li>
                    <li><a href="cerrarSesion.php">Cerrar sesión</a></li>
                   
                </ul>
            </nav>
            <article>
             <div class="container">
                <div class="msg" id="msg">
                </div>
                     <div class="formuario">
                        <form action="/Proyecto 1 - My Trees/crearUsuario.php" onsubmit="return validateStudentForm();" method="POST" class="form-inline" role="form">

                          <input type="hidden" name="id" value="<?php echo $student['id']?>">

                          <div class="form-group">
                            <label class="sr-only" for="">especie</label>
                            <input type="text" class="form-control" id="" name="especie" placeholder="especie" disabled value="<?php echo $student['especie'] ?>">
                          </div>
                          <br><br>
                          <div class="form-group">
                            <label class="sr-only" for="">nombre</label>
                            <input type="text" class="form-control" id="" name="nombre" placeholder="nombre" disabled value="<?php echo $student['nombre'] ?>">
                          </div>
                          <br><br>
                          <?php
                              session_start();
                              $user = $_SESSION['user'];
                              if (!$user) {
                                header('Location: /Proyecto 1 - My Trees/index.php');
                              }
                          ?>
                          
                          <div class="form-group">
                            <label class="sr-only" for="">nombre</label>
                            <input type="text" class="form-control" id="" name="nombreDoador" placeholder="nombreDoador" disabled value="<?php echo $user['nombre'] ?>">
                          </div>
                          <br><br>
                          
                          <div class="form-group">
                            <label class="sr-only" for="">donar</label>
                            <input type="number" class="form-control" id="" name="nombreDoador" placeholder="nombreDoador" >
                          </div>
                          <br><br>
                          <img src="<?php echo $student['imagen']?>"  >
        
                          <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                          </div>
                    </div>
            </article>
        </div>
    </main>
    </body>
</html>