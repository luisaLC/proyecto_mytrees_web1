<?php
  include('functions.php');

  if($_REQUEST['id']) {
    $student = getArbol($_REQUEST['id']);
       
  }

  if($_POST){
    if ($filename = uploadPicture('picture')){
     
      $student['imagen'] = $filename;
      $student['especie'] = $_POST['especie'];
      $student['nombre'] = $_POST['nombre'];
      guardarArbolNuevo($student);
    } else {
      echo "There was an error saving the picture";
    }
  }
?>

<!DOCTYPE html>
    <html lang="en">
    <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <meta http-equiv="X-UA-Compatible" content="ie=edge">
          <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
          <title>Donar Arbol</title>
    </head>
    <body>
          <div class="container">
                <div class="msg" id="msg">
                  <?php echo $message; ?>
                </div>
                 
                  <?php
                          session_start();
                          $user = $_SESSION['user'];
                          if (!$user) {
                            header('Location: /auth/index.php');
                          }
                  ?>

                <h1><?php echo $user['nombre'] ?>, bienvenido a la tienda My Trees </h1>
    
                        <form method="POST" class="form-inline" role="form" enctype="multipart/form-data">

                        <input type="hidden" name="id" value="<?php echo $student['id']?>">

                        <div class="form-group">
                            <label class="sr-only" for="">especie</label>
                            <input type="text" class="form-control" id="" name="especie" placeholder="especie" value="<?php echo $student['especie'] ?>">
                        </div>
                                
                        <div class="form-group">
                            <label class="sr-only" for="">nombre</label>
                            <input type="text" class="form-control" id="" name="nombre" placeholder="nombre" value="<?php echo $student['nombre'] ?>">
                        </div>

                        <input type="file" name="picture" id="picture">

                        <img src="<?php echo $student['imagen']?>">


                        <button type="submit" class="btn btn-primary">Save</button>
                </form>
      </div>

</body>
</html>