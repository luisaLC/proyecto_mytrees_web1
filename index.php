<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Publico</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/index.css">
    <link rel="shortcut icon" href="img/fIcon4.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js"></script> 
</head>

<body >
   <body >
   
    <div class="contenedor">
        <nav class="navbar navbar-expand-sm bg-pink navbar-light">
            <a class="navbar-brand" href="#">
                <img src="img/logo1.png" alt="Logo" style="width:20%;">
            </a>
            
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="iniciarSesion.php">| Iniciar Sesion |</a>
                </li>
                   <li class="nav-item">
                    <a class="nav-link" href="registrarse.php">| Registrarse |</a>
                </li>
                
            </ul>
        </nav>
    </div>
    <div class="container mt-2">
        <div class="d-flex mb-3">
            <div class="p2 flex-fill">
                <div class="row">
                   
                    <div class="col-">
                        <div class="principal">
                             <h2> <a>¡Bienvenidos a Fundación My Trees! </a> </h2>
                              <img src="img/arbol.jpg" alt="Logo" style="width:100%;">
                               <BR></BR>
                           <h2> <a>¡Donaciones Online! </a> </h2>
                                <p>
                                Si deseas ayudarnos brindando una donación, puedes hacerlo aquí. Te estaremos enviando fotos de tu árbol adoptado.
                               </p>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="cuentas">
                              <img src="img/arbol.jpg" alt="Logo" style="width:100%;">
                               <BR></BR>
                               <div class="textos">
                           <h2> <a>Siembras Voluntarias</a> </h2>
                                <p>
                                Si quieres ser voluntario y asistir a nuestras siembras completamente gratis, regístrate en este link. ¡Vamos a sembrar árboles!
                               </p>
                            </div>
                        </div>
                    </div>
                    
                     <div class="col-sm">
                        <div class="cuentas">
                              <img src="img/arbol.jpg" alt="Logo" style="width:100%;">
                               <BR></BR>
                           <h2> <a>Siembras Empresariales</a> </h2>
                                <p>
                                Haz que todos los miembros de tu organización vivan una experiencia de sensibilización y trabajo colectivo. ¡Siembra con nosotros!
                               </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


  

</body>

</html>