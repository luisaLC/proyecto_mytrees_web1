<?php
  require('functions.php');

  $message = "";
  if(!empty($_REQUEST['status'])) {
    switch($_REQUEST['status']) {
      case 'success':
        $message = 'User was added succesfully';
      break;
      case 'error':
        $message = 'There was a problem inserting the user';
      break;
    }
  }
?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Perfil</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/perfil.css">
    <link rel="shortcut icon" href="img/fIcon4.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="js/buscar.js"> </script>
</head>

<body >
    <main>
        <div class="content-all">
            <header> </header>
            <input type="checkbox" id="check">
            <label for="check" class="icon-menu">Menu</label>
            
           
            
            <nav class="menu">
                <ul>
                    <li><a href="index.php">Inicio</a></li>
                    <li><a href="">Configuración</a></li>
                    <li><a href="">Mis arboles</a></li>
                    <li><a href="tienda.php">Tienda de arboles</a></li>
                    <li><a href="cerrarSesion.php">Cerrar sesión</a></li>
                   
                </ul>
            </nav>
            
           
            
            <article>
                 
                 <h3>Mis arboles</h3>
                 
         
                  <table class="table table-light">
      <tbody>
        <tr>
          <td>Nombre</td>
          <td>Fecha compra</td>
          <td>Detalles</td>
          <td>Ubicación</td>
        </tr>
         <?php
          $arboles = getArboles();
          $arbolesHtml = "";
          foreach ($arboles as $arbol) {
            $arbolesHtml .= 
            "<tr 
            id='arbol_{$arbol['id_arboles']}'>
            <td>{$arbol['nombre']}</td>
            <td>{$arbol['fecha']}</td>
            <td>{$arbol['detalles']}</td>
            <td>{$arbol['ubicacion']}</td>
            <td><a href='deleteStudent.php?id={$arbol['id_arboles']}' class='btn btn-primary' onclick='deleteStudent.php({$arbol['id_arboles']})'>Delete</a>
            </td>
            </tr>";
          }
          echo $arbolesHtml;
        ?>
        
      </tbody>
    </table>
            </article>
            
        </div>
    </main>
    </body>
</html>