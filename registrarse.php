<?php
  require('functions.php');

  $message = "";
  if(!empty($_REQUEST['status'])) {
    switch($_REQUEST['status']) {
      case 'success':
        $message = 'User was added succesfully';
      break;
      case 'error':
        $message = 'There was a problem inserting the user';
      break;
    }
  }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Resgistro de Usuario</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/registrarse.css">
    <link rel="shortcut icon" href="img/fIcon4.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="js/buscar.js"> </script>
</head>

<body >
   <body >
   
    <div class="contenedor">
        <nav class="navbar navbar-expand-sm bg-pink navbar-light">
            <a class="navbar-brand" href="#">
                <img src="img/logo1.png" alt="Logo" style="width:20%;">
            </a>
            
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="iniciarSesion.php">| Iniciar Sesion |</a>
                </li>
                   <li class="nav-item">
                    <a class="nav-link" href="index.php">| Inicio |</a>
                </li>
                
            </ul>
        </nav>
    </div>
    <div class="container mt-3">
        <div class="d-flex mb-3">
            <div class="p2 flex-fill">
                <div class="row">
                    <div class="col-sm">
                        <div class="cuentas">
                              <img src="img/arbol.jpg" alt="Logo" style="width:100%;">
                               <BR></BR>
                           <h2> <a>¡Donaciones Online! </a> </h2>
                                <p>
                                Si deseas ayudarnos brindando una donación, puedes hacerlo aquí. Te estaremos enviando fotos de tu árbol adoptado.
                               </p>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="formuario">
                               <form action="/Proyecto 1 - My Trees/crearUsuario.php" onsubmit="return validateStudentForm();" method="POST" class="form-inline" role="form">
                               <h2>Registro de usuario</h2>
                                <input class="input" type="text" id="nombre" name="nombre" placeholder=" Nombre " required autofocus>
                                <input class="input" type="text" id="apellido" name="apellido"  placeholder=" Apellido" required autofocus>
                                <input class="input" type="text" id="telefono" name="telefono"  placeholder=" Telefono" required autofocus>
                                <input class="input" type="text" id="correo" name="correo"  placeholder=" Correo" required autofocus>
                                <input class="input" type="text" id="direccion" name="direccion"  placeholder=" Dirección" required autofocus>
                                <input class="input" type="text" id="pais" name="pais"  placeholder=" Pais" required autofocus>
                                <input class="input" type="password" id="contraseña" name="contraseña"  placeholder=" Contraseña" required autofocus>
                                <h4>Ya tienes cuenta?  <a href="iniciarSesion.html" id="reg"> Inicia Sesión </a></h4>
                                   <div class="btn__form">
                                     <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      

    <script src="js/jquery.js"></script>
      <script src="js/mostrarDash.js"></script>
       <script src="js/datos.js"></script>
    <script src="js/bootstrap.min.js"></script>
    
    <script>
    loadTableDataa('nuevo');
  </script>


</body>

</html>