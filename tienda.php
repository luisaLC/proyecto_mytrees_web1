<?php
  require('functions.php');

  $message = "";
  if(!empty($_REQUEST['status'])) {
    switch($_REQUEST['status']) {
      case 'success':
        $message = 'User was added succesfully';
      break;
      case 'error':
        $message = 'There was a problem inserting the user';
      break;
    }
  }
?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tienda</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/tienda.css">
    <link rel="shortcut icon" href="img/fIcon4.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  
</head>

<body >
    <main>
        <div class="content-all">
            <header> </header>
            <input type="checkbox" id="check">
            <label for="check" class="icon-menu">Menu</label>
            
           
            
            <nav class="menu">
                <ul>
                    <li><a href="index.php">Inicio</a></li>
                    <li><a href="">Configuración</a></li>
                    <li><a href="">Mis arboles</a></li>
                    <li><a href="tienda.php">Tienda de arboles</a></li>
                    <li><a href="cerrarSesion.php">Cerrar sesión</a></li>
                   
                </ul>
            </nav>
            <article>
             <?php
                  session_start();

                  $user = $_SESSION['user'];
                  if (!$user) {
                    header('Location: /Proyecto 1 - My Trees/index.php');
                  }
                  ?>

                  <h1><?php echo $user['nombre'] ?>, Bienvenido a la tienda My Trees </h1>
              <table class="table table-light">
                  <tbody>
                    <tr>
                      <td>Arbol</td>
                      <td>Especie</td>
                      <td>Nombre</td>
                      <td>Donar</td>
                    </tr>
                    <?php
                      $students = getProductos();
                      $studentsHtml = "";
                      foreach ($students as $student) {
                        $studentsHtml .= 
                        "<tr id='student_{$student['id']}'>
                        <td ><img src='{$student['imagen']}' width='60%' height='15%'> </td>
                        <td >{$student['especie']}</td>
                        <td >{$student['nombre']}</td>
                        <td><a href='guardaArbol.php?id={$student['id']}'>Edit</a></td>
                        </tr>";
                      }
                      echo $studentsHtml;
                    ?>
                  </tbody>
                </table>
            </article>
            
        </div>
    </main>
    </body>
</html>