<?php
  require('functions.php');

  $message = "";
  if(!empty($_REQUEST['status'])) {
    switch($_REQUEST['status']) {
      case 'success':
        $message = 'User was added succesfully';
      break;
      case 'error':
        $message = 'There was a problem inserting the user';
      break;
    }
  }
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Publico</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/tiendaPrueba.css">
    <link rel="shortcut icon" href="img/fIcon4.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js"></script> 
</head>

<body >
   <body >
   
    <div class="contenedor">
        <nav class="navbar navbar-expand-sm bg-pink navbar-light">
            <a class="navbar-brand" href="#">
                <img src="img/logo1.png" alt="Logo" style="width:20%;">
            </a>
            
            
        </nav>
        
        <header> </header>
            <input type="checkbox" id="check">
            <label for="check" class="icon-menu">Menu</label>
            
           
            
            <nav class="menu">
                <ul>
                    <li><a href="index.php">Inicio</a></li>
                    <li><a href="">Configuración</a></li>
                    <li><a href="">Mis arboles</a></li>
                    <li><a href="tienda.php">Tienda de arboles</a></li>
                    <li><a href="cerrarSesion.php">Cerrar sesión</a></li>
                   
                </ul>
            </nav>
    </div>
    <div class="container mt-2">
        <div class="d-flex mb-3">
            <div class="p2 flex-fill">
                <div class="row">
                 
                    <div class="col-sm">
                        <div class="cuentas">
                             <?php
                  session_start();

                  $user = $_SESSION['user'];
                  if (!$user) {
                    header('Location: /auth/index.php');
                  }
                  ?>

                  <h1><?php echo $user['nombre'] ?>, Bienvenido a la tienda My Trees </h1>
              <table class="table table-light">
                  <tbody>
                    <tr>
                      <td>Arbol</td>
                      <td>Nombre</td>
                      <td>Marca</td>
                      <td>Stock</td>
                      <td>Donar</td>
                    </tr>
                    <?php
                      $students = getProductos();
                      $studentsHtml = "";
                      foreach ($students as $student) {
                        $studentsHtml .= 
                        "<tr>
                        <td><img src='{$student['Imagen']}' width='60%' height='15%'> </td>
                        <td>{$student['NombreProd']}</td>
                        <td>{$student['Marca']}</td>
                        <td>{$student['Stock']}</td>
                        <td><a href='' class='btn btn-primary' onclick=''>Delete</a></td>
                        </tr>
                        <td></td>";
                         
                      }
                      echo $studentsHtml;
                    ?>
                  </tbody>
                </table>
                        </div>
                    </div>
                    
                     <div class="col-sm">
                        <div class="cuentas">
                              <form method="POST" class="form-inline" role="form" enctype="multipart/form-data">
                              <input type="hidden" name="id" value="<?php echo $student['NombreProd']?>">
                              <div class="form-group">
                                <label class="sr-only" for="">Full Name</label>
                                <input type="text" class="form-control" id="" name="full_name" placeholder="Full Name" value="<?php echo $student['fullname'] ?>">
                              </div>
                              <div class="form-group">
                                <label class="sr-only" for="">Email</label>
                                <input type="email" class="form-control" id="" name="email" placeholder="Email" value="<?php echo $student['email'] ?>">
                              </div>
                              <input type="file" name="picture" id="picture">
                              <img src="<?php echo $student['profilePic']?>">
                              <select name="carrera" id="carrera" >
                                <option value="<?php echo $student['carreras']?>">Seleccione:</option>
                                 <?php
                                   $careers = getCarreras();
                                   $careerHtml = "";
                                   foreach ($careers as $valores) {
                                       if($valores[id] == $student['carreras']){
                                          $careerHtml .= '<option selected="true" value="'.$valores[id].'">'.$valores[carreras].'</option>';   
                                       }else{
                                          $careerHtml .= '<option value="'.$valores[id].'">'.$valores[carreras].'</option>'; 
                                       }
                                  }
                                     echo  $careerHtml;
                                     ?>
                              </select>

                              <button type="submit" class="btn btn-primary">Save</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


  

</body>

</html>